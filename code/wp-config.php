<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'propertyroom');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Vl@zkC6&l|vQIq~T]/I`3]Ze4>I}+P5d+PT@~~3eF57(vQmILCYmBclp93tyR_+$');
define('SECURE_AUTH_KEY',  'XJp9zJI-!2Gx08jwT32-Y9~m,AUbeudK [,^[%fKXK0H||u?Ota]z8GXYQ)Wu+1l');
define('LOGGED_IN_KEY',    'ViyjN.}Q3t74:McLB-@tW y:-zF}NAN? sla)c+=8p#R#=Ar~$|Mfg1^2-mO0kmd');
define('NONCE_KEY',        'fEf$j;h&b:Gp(MjdBO]N^joLCh05vcZ7|OMbv?nS2G~2+/0yP8^6fdm{xEs^GM+l');
define('AUTH_SALT',        'm~o}9c}6/uUpmWqh/^H/<7F5O:$&fk{LIzo-Iq,VB-]9y#y@i-?s(|8y5nv0ONaj');
define('SECURE_AUTH_SALT', 'O7fYHzsK+v-:D&fv%CWcyuX+gZvTODs2ASxZ3mnReS:B-/!+EJOc$I1Gs|7pjh!?');
define('LOGGED_IN_SALT',   'E:IG(& >+4)plGRk5t1_@2F5W4p7>Xfw^$aKL$[*1wf8IKXX@8}yjkO$!i%Phjmq');
define('NONCE_SALT',       '-+I *I%ML@|sL$G-[~ejBFIC|[PZa,z|)p^0guWLE~U>3w,h`]|=a+f3Q5BUbA^u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
