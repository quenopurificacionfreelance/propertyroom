$(window).load(function(){   
    
$('h1').click(function(){
    $(this).addClass('slideDown');
})


    setInterval(function() {        
        $('.flexslider ul.slides li h2').removeClass('fadeInDown');
        $('.flexslider ul.slides li.flex-active-slide h2').addClass('fadeInDown');
        $('.flexslider ul.slides li span').removeClass('fadeInLeft');
        $('.flexslider ul.slides li.flex-active-slide span').addClass('fadeInLeft');
        $('.flexslider ul.slides li img').removeClass('fadeInRight');
        $('.flexslider ul.slides li.flex-active-slide img').addClass('fadeInRight');
    },100);

});

$(window).load(function(){ 

  // init Isotope
  var $container = $('.isotope').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',
      weight: function( itemElem ) {
        var weight = $( itemElem ).find('.weight').text();
        return parseFloat( weight.replace( /[\(\)]/g, '') );
      }
    }
  });

  // filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };

  // bind filter button click
  
  $('#filters').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
    //$('ul.slides.isotope').css('width', 'auto');
  });

  


  // bind sort button click
  $('#sorts').on( 'click', 'button', function() {
    var sortByValue = $(this).attr('data-sort-by');
    $container.isotope({ sortBy: sortByValue });
  });
  
  // change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });
  // setTimeout(function() {        
  //     $container.isotope({ filter: '*' });
  // },100);


  
});