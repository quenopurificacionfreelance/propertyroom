<?php
register_nav_menus( array(
	'navigation_menu_left' => 'Header Navigation Menu Left',	
	'navigation_menu_right' => 'Header Navigation Menu Right',	
	'footer_menu' => 'Footer Menu',
) );

function post_thumbnail() {
 //Featured Image
 //add_theme_support('post-thumbnails');
 //add_image_size( 'full-width-crop', 241, 153, true ); 
}
add_action('after_setup_theme', 'post_thumbnail');

// Add Featured image
add_theme_support( 'post-thumbnails' ); 

function widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Search', 'propertyroom' ),
		'id'            => 'search',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<span class="hidden">',
		'after_title'   => '</span>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer', 'propertyroom' ),
		'id'            => 'footer',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h2 class="front-tag">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'widgets_init' );

function wpdocs_custom_excerpt_length( $length ) {
    return 8;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function wpdocs_excerpt_more( $more ) {
    return sprintf( '<br/><a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Bid now', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


//require get_template_directory() . '/include/cpt-slider.php';
//require get_template_directory() . '/include/cpt-solutions-pathways.php';

//require get_template_directory() . '/include/acf-options-cpt.php';