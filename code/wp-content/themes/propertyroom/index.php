<?php get_header(); ?>
	<header>
	
	</header>

	<?php		
		if( have_rows('modules') ):		 	
		 	//$counter = 1;	
		    while ( have_rows('modules') ) : the_row();	
		    	if( get_row_layout() == 'post_content' ):
		    		include 'include/acf-post-content.php';
		    	endif;			
				

				if( get_row_layout() == 'featured_banner_repeater' ):
					if( have_rows('repeater_content') ): 
						include 'include/acf-banner-repeater.php';
					endif;        
				endif;
		    endwhile;
		endif;

	?>
					
<?php get_footer(); ?>