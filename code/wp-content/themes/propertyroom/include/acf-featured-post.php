<?php $terms = get_sub_field('post_category'); ?>
	<div class="carousel-products desktop">	
		<div class="container relative">
			<div class="carousel-nav">	
    			<div id="filters" class="button-group">  
    				<!-- <button class="button is-checked showall" data-filter="*">show all</button> -->
					<?php 
					if( $terms ):
						$cnt = 1;
						foreach( $terms as $term ):									
							$category = $term->term_id .",";	
							$termname = $term->name;
							$catdash = sanitize_title_with_dashes( $termname, $unused, $context = 'display' );	
							$catlow = strtolower($catdash);
							if($cnt == 1) :
								echo "<span><button class='button is-checked' data-filter='.".$catlow."'>".$term->name."</button></span>";				
							else :
								echo "<span><button class='button' data-filter='.".$catlow."'>".$term->name."</button></span>";
							endif;	
							$cnt++;		
						endforeach;
					endif; ?>
				</div>
			</div>	        			
			<?php 
			if( $terms ):				
				foreach( $terms as $term ):									
					$category = $term->term_id .",". $category;											
				endforeach;
			endif; ?>
			<?php
			//echo $category;
			$args2 = array(
				'posts_per_page' => 999,		
				'order' => ASC,
				'cat' => $category
			);
			query_posts($args2); ?>
			<section class="slider">
		        <div class="flexslider-carousel">
		          <ul class="slides">
		          	<?php while (have_posts()) : the_post(); 	
		          		$pcat = get_the_category();
		          		if( $pcat ):
							foreach( $pcat as $catterm ):									
								$category = $catterm->term_id .",". $category;	
								$ptermname = $catterm->name;
								$pcatdash = sanitize_title_with_dashes( $ptermname, $unused, $context = 'display' );	
								$pcatlow = strtolower($pcatdash);	
								//echo $pcatlow;					
							endforeach;
						endif; ?>			          		
						<li class="col-md-3 col-sm-3 col-xs-12 <?php echo $pcatlow; ?>">
	            			<div class="box">
		  	    	    		<?php 
		  	    	    			if ( has_post_thumbnail() ) :
										the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive animated"));
									endif;
		  	    	    		?>
		  	    	    	</div>											  	    	    	
	  	    	    		<div class="content-text">
	  	    	    		<?php 
	  	    	    			$posttitle = get_the_title(); 
	  	    	    			echo "<h4>".$posttitle."</h4>";									  	    	    		
	  	    	    			the_excerpt(); ?>
	  	    	    		</div>
		  	    		</li>
		  	    	<?php endwhile; ?>
		          </ul>
		        </div>
		    </section>
		</div>							
	</div>



	<div class="carousel-products mobile">	
		<div class="relative">
			<div class="carousel-nav">	
    			<div id="filters" class="button-group">  
    				<!-- <button class="button is-checked showall" data-filter="*">show all</button> -->
					<?php 
					if( $terms ):
						$cnt = 1;
						foreach( $terms as $term ):									
							$category = $term->term_id .",";	
							$termname = $term->name;
							$catdash = sanitize_title_with_dashes( $termname, $unused, $context = 'display' );	
							$catlow = strtolower($catdash);
							if($cnt == 1) :
								echo "<span><button class='button is-checked' data-filter='.".$catlow."'>".$term->name."</button></span>";				
							else :
								echo "<span><button class='button' data-filter='.".$catlow."'>".$term->name."</button></span>";
							endif;	
							$cnt++;		
						endforeach;
					endif; ?>
				</div>
			</div>	        			
			<?php 
			if( $terms ):
				$cnt = 1;
				foreach( $terms as $term ):									
					$category = $term->term_id .",". $category;											
				endforeach;
			endif; ?>
			<?php
			//echo $category;
			$args2 = array(
				'posts_per_page' => 4,		
				'order' => ASC,
				'cat' => $category
			);
			query_posts($args2); ?>
			
	        <div class="featuredpost">
	          <div class="mobile">
	          	<?php while (have_posts()) : the_post(); 	
	          		$pcat = get_the_category();
	          		if( $pcat ):
						foreach( $pcat as $catterm ):									
							$category = $catterm->term_id .",". $category;	
							$ptermname = $catterm->name;
							$pcatdash = sanitize_title_with_dashes( $ptermname, $unused, $context = 'display' );	
							$pcatlow = strtolower($pcatdash);	
							//echo $pcatlow;					
						endforeach;
					endif; ?>	
            			<div class="box col-xs-4 col-sm-4 col-md-4">
            				<span class="border-right">
	  	    	    		<?php 
	  	    	    			if ( has_post_thumbnail() ) :
									the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive animated"));
								endif;
	  	    	    		?>
	  	    	    		</span>
	  	    	    	</div>											  	    	    	
  	    	    		<div class="content-text col-xs-8 col-sm-8 col-md-8">
  	    	    		<?php 
  	    	    			$posttitle = get_the_title(); 
  	    	    			echo "<h4>".$posttitle."</h4>";									  	    	    		
  	    	    			the_excerpt(); ?>
  	    	    		</div>
	  	    			<div class="clearfix"></div>
	  	    	<?php endwhile; ?>
	          </div>
	        </div>
		</div>							
	</div>
<?php