<?php
if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();		
        if( get_row_layout() == 'featured_post' ):                 
            include 'acf-featured-post.php';
        endif;

        if( get_row_layout() == 'full_text' ): ?>
            <div class="fulltext"> <?php 
                $content = get_sub_field('fulltext_content'); 
                echo $content; ?>
            </div><?php
        endif;
    endwhile;
endif; ?>