<div class="banner">
	<div id="main" role="main">
		<section class="slider">
	        <div class="flexslider">
	          	<ul class="slides">
					<?php
					while ( have_rows('repeater_content') ) : the_row();
						$banner_title = get_sub_field('banner_title');
						$banner_text = get_sub_field('banner_text');	
						$banner_link = get_sub_field('banner_link');
						$banner_image = get_sub_field('banner_image'); ?>
							<li>			            	
			            		<div class="container relative">
				  	    	    	<div class="col-md-5 col-sm-5 col-xs-5 no-position">
				  	    	    		<div class="content-text">
				  	    	    		<?php 					  	    	    			
				  	    	    			echo "<h2 class='animated2'>".$banner_title."</h2>";
				  	    	    			echo "<span class='animated2'>";
				  	    	    			echo $banner_text;
				  	    	    			if($banner_link) :
				  	    	    				echo "<a href='".$banner_link."' >Shop now</a>";
				  	    	    			endif;
				  	    	    			echo "</span>"; ?>
				  	    	    		</div>
				  	    	    	</div>
				  	    	    	<div class="col-md-7 col-sm-7 col-xs-7">
				  	    	    		<div class="row">
					  	    	    		<?php 
					  	    	    			if ($banner_image) : ?>
													<img src="<?php echo $banner_image; ?>" class="img-responsive animated" /> <?php
												endif;
					  	    	    		?>
				  	    	    		</div>
				  	    	    	</div>
			  	    	    	</div>
			  	    		</li>										  	    	
					<?php										          
					endwhile; ?>
				</ul>
	        </div>
	    </section>
	</div>							
</div>