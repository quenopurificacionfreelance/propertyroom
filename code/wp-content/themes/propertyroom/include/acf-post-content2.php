<?php
if( have_rows('content') ):
	while ( have_rows('content') ) : the_row();		
        if( get_row_layout() == 'featured_post' ): //echo "test";
        	$terms = get_sub_field('post_category'); ?>
        		<div class="carousel-products">	
        			<div class="container relative">
	        			<div class="carousel-nav">	
		        			<div id="filters" class="button-group">  
		        				<!-- <button class="button is-checked showall" data-filter="*">show all</button> -->
								<?php 
								if( $terms ):
									$cnt = 1;
									foreach( $terms as $term ):									
										$category = $term->term_id .",". $category;	
										$termname = $term->name;
										$catdash = sanitize_title_with_dashes( $termname, $unused, $context = 'display' );	
										$catlow = strtolower($catdash);
										if($cnt == 1) :
											echo "<button class='button is-checked' data-filter='.".$catlow."'>".$term->name."</button>";								
										else :
											echo "<button class='button' data-filter='.".$catlow."'>".$term->name."</button>";
										endif;	
										$cnt++;		
									endforeach;
								endif; ?>
							</div>
						</div>
						<?php
						//echo $category;
						$args2 = array(
							'posts_per_page' => 999,		
							'order' => ASC,
							'cat' => $category
						);
						query_posts($args2); ?>
						<section class="slider">
					        <div class="flexslider-carousel">
					          <ul class="slides isotope">
					          	<?php while (have_posts()) : the_post(); 	
					          		$pcat = get_the_category();
					          		if( $pcat ):
										foreach( $pcat as $catterm ):									
											$category = $catterm->term_id .",". $category;	
											$ptermname = $catterm->name;
											$pcatdash = sanitize_title_with_dashes( $ptermname, $unused, $context = 'display' );	
											$pcatlow = strtolower($pcatdash);	
											//echo $pcatlow;					
										endforeach;
									endif; ?>				          		
									<li class="col-md-3 col-sm-3 col-xs-12 element-item <?php echo $pcatlow; ?>">
				            			<div class="box">
					  	    	    		<?php 
					  	    	    			if ( has_post_thumbnail() ) :
													the_post_thumbnail('post-thumbnail', array( 'class'	=> "img-responsive animated"));
												endif;
					  	    	    		?>
					  	    	    	</div>											  	    	    	
				  	    	    		<div class="content-text">
				  	    	    		<?php 
				  	    	    			$posttitle = get_the_title(); 
				  	    	    			echo "<h4>".$posttitle."</h4>";									  	    	    		
				  	    	    			the_excerpt(); ?>
				  	    	    		</div>
					  	    		</li>
					  	    	<?php endwhile; ?>
					          </ul>
					        </div>
					    </section>
					</div>							
				</div>
			<?php
        endif;

        if( get_row_layout() == 'full_text' ): ?>
        	<div class="fulltext"> <?php 
        		$content = get_sub_field('fulltext_content'); 
        		echo $content; ?>
        	</div>
        <?php
        endif;
	endwhile;
endif; ?>